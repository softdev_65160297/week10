package com.mycompany.databaseproject;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author paewnosuke
 */
public class SelectDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";

        //conect db
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //select
        String sql = "SELECT * FROM catagory";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("cat_id") + " " + rs.getString("cat_name"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //close db
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println((ex.getMessage()));
            }
        }

    }

}
